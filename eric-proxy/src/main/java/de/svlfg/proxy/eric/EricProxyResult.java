package de.svlfg.proxy.eric;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

public interface EricProxyResult {
	void write(HttpServletResponse resp)throws ServletException ;
}
