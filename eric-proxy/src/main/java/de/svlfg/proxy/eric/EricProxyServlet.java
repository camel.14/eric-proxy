package de.svlfg.proxy.eric;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EricProxyServlet extends HttpServlet {
	private final static Logger log = LoggerFactory.getLogger(EricProxyServlet.class);


	@Override
	protected void doPost(HttpServletRequest servletRequest, HttpServletResponse resp) throws ServletException, IOException {
		EricProxyRequest ericRequest = new EricProxyRequest(Activator.getProxyConfig(),Eric.getLibrary());

		ericRequest.readVerfahrenDatenartTestmerker(servletRequest.getPathInfo());
		log.debug("doPost content-type: {}",servletRequest.getContentType());
		readRequestBody(servletRequest, ericRequest);
		try {
			EricProxyResult result = ericRequest.sendRequest();
			result.write(resp);
		} catch (EricProxyException e) {
			((EricProxyResult)e).write(resp);
		}
	}


	private void readRequestBody(HttpServletRequest req, EricProxyRequest request)
			throws IOException, ServletException {
		try {
			XmlUtils xmlUtils = XmlUtils.getInstance();
			SOAPMessage message = xmlUtils.readSoapMessage(req);
			request.setBody(xmlUtils.toString(xmlUtils.selectFirstBodyElement(message)));
			request.setTransferHandle(xmlUtils.readTransferHandle(message));
		} catch (SOAPException e) {
			throw new ServletException(e);
		}
	}


	@Override
	public void init() throws ServletException {
		super.init();
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
