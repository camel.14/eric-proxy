package de.svlfg.proxy.eric;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.AbstractMap.SimpleEntry;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import de.svlfg.eric.library.EricapiLibrary;
import de.svlfg.proxy.eric.config.EricProperties;
import de.svlfg.proxy.eric.config.UserConfig;

public class Activator implements BundleActivator{

	private static UserConfig proxyConfig;
	private static EricCertificateProvider certificateProvider;

	public static EricCertificateProvider getCertificateProvider() {
		return certificateProvider;
	}

	@Override
	public void start(BundleContext context) throws Exception {
		initProxyConfig(context);
		initEric(context);
		initCertificateProvider();
	}

	private void initCertificateProvider() {
		certificateProvider = new EricCertificateProvider(Activator.getProxyConfig(),Eric.getLibrary());
		
	}

	private void initProxyConfig(BundleContext context) throws InvalidSyntaxException {
		Optional<EricProperties> ericProperties = loadEricProperties(context);
		proxyConfig = ericProperties.orElseThrow(()->new RuntimeException()).getUserConfig();
		
	}
	private Optional<EricProperties> loadEricProperties(BundleContext context) throws InvalidSyntaxException {
		Collection<ServiceReference<EricProperties>> ericPropertiesReference = context
				.getServiceReferences(EricProperties.class, null);
		Optional<EricProperties> ericProperties = ericPropertiesReference.stream()//
				.map(reference -> new SimpleEntry<Object, ServiceReference<EricProperties>>(
						context.getProperty("priority"), reference)) //
				.map(e -> new SimpleEntry<String, ServiceReference<EricProperties>>(
						e.getKey() == null ? "100" : e.getKey().toString(), e.getValue())) //
				.sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey())) //
				.map(e -> context.getService(e.getValue())).findFirst();
		return ericProperties;
	}

	private void initEric(BundleContext context) {
		ServiceReference<EricapiLibrary> reference = context.getServiceReference(EricapiLibrary.class);
		EricapiLibrary ericapiLibrary = context.getService(reference);
		Objects.requireNonNull(ericapiLibrary, "The EricapiLibrary must not be null!");
		Eric.initialize(ericapiLibrary);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		certificateProvider.release();
		certificateProvider = null;
	}

	public static UserConfig getProxyConfig() {
		return proxyConfig;
	}

}
