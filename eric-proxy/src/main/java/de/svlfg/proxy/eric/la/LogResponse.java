package de.svlfg.proxy.eric.la;

import java.io.IOException;
import java.io.Writer;

public class LogResponse {

	private Writer out;
	private LogAccess logAccess;

	public LogResponse(LogAccess logAccess,Writer out) {
		this.out = out;
		this.logAccess = logAccess; 
	}

	public void write() throws IOException {
		 out.append("<div>");
		 out.append("<form method=\"GET\">");
		 out.append("Pattern: <input type=\"text\" name=\"pattern\" value=\"").append(logAccess.getPattern()).append("\"></input>");
		 out.append("<input type=\"submit\" value=\"Submit\"></input>");
		 out.append("</form>");
		 out.append("</div>");
		 out.append("<div><pre>");
		 out.append(logAccess.read());
		 out.append("</pre></div>");
	}
	
}
