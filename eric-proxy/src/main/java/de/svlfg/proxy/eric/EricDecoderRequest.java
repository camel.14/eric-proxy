package de.svlfg.proxy.eric;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;

import de.svlfg.eric.library.EricapiLibrary;
import de.svlfg.proxy.eric.config.UserConfig;

public class EricDecoderRequest extends EricAbstractRequest {
	final private EricapiLibrary ericapiLib;
	private final static Logger log = LoggerFactory.getLogger(EricDecoderRequest.class);
	private final static StringEncoding STRDEC = new StringEncoding();

	public EricDecoderRequest(UserConfig proxyConfig, EricapiLibrary ericapiLib) {
		super(proxyConfig);
		this.ericapiLib = ericapiLib;
	}

	public String getDecoderContent() {
		return decoderContent;
	}

	public void setDecoderContent(String decoderContent) {
		this.decoderContent = decoderContent;
	}

	private String decoderContent;
	
	public EricProxyResult decode() throws IOException {
		IntByReference certificateHandle = Activator.getCertificateProvider().getCertificateHandle();

		PointerByReference rueckgabePuffer = ericapiLib.EricRueckgabepufferErzeugen();

		int rc = ericapiLib.EricDekodiereDaten(certificateHandle.getValue(), this.getUserConfig().getCertificatePin(),
				this.getDecoderContent(), rueckgabePuffer);
		if (rc != 0) {
			throw new RuntimeException("Error loading public key! " + rc);
		}

		EricProxyResultSuccess ericProxyResultSuccess = new EricProxyResultSuccess();
		String ericRueckgabepufferInhalt = ericapiLib.EricRueckgabepufferInhalt(rueckgabePuffer);
		ericRueckgabepufferInhalt = STRDEC.decode(ericRueckgabepufferInhalt);
		log.debug("after decoding\n{}",ericRueckgabepufferInhalt);
		ericProxyResultSuccess.setBody(ericRueckgabepufferInhalt.getBytes());
		return ericProxyResultSuccess;
	}

	
}
