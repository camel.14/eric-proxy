package de.svlfg.proxy.eric;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class IOUtils {
	public static  String toString(InputStream ris) throws IOException {
		ByteArrayOutputStream bos = toByteArrayOutputStream(ris);
		return bos.toString();
	}
	
	public static byte[] toBytes(InputStream ris) throws IOException {
		ByteArrayOutputStream bos = toByteArrayOutputStream(ris);
		return bos.toByteArray();
	}

	private static ByteArrayOutputStream toByteArrayOutputStream(InputStream ris) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		copyStream(ris, bos);
		ris.close();
		bos.close();
		return bos;
	}

	public static void copyStream(InputStream ris, OutputStream bos) throws IOException {
		byte[] b = new byte[4096];
		int len, off = 0;
		while ((len = ris.read(b)) > 0) {
			bos.write(b, off, len);
		}
	}

}
