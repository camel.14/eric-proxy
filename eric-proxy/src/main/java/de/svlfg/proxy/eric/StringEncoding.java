package de.svlfg.proxy.eric;

import java.io.UnsupportedEncodingException;

public class StringEncoding {
	public String decode(String s) throws UnsupportedEncodingException {
		return new String(s.getBytes("iso-8859-15"));
	}
}
