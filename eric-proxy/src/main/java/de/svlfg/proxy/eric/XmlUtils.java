package de.svlfg.proxy.eric;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

public class XmlUtils {

	private static final XmlUtils INSTANCE = new XmlUtils();
	private final static Logger log = LoggerFactory.getLogger(XmlUtils.class);

	private XmlUtils() {

	}
	public SOAPMessage readSoapMessage(HttpServletRequest req)
			throws IOException, ServletException {
		try {
			MimeHeaders headers = new MimeHeaders();
			headers.setHeader("content-type", req.getContentType());
			SOAPMessage message = MessageFactory.newInstance().createMessage(headers , req.getInputStream());
			return message;

		} catch (SOAPException e) {
			throw new ServletException(e);
		}
	}
	
	public Element selectFirstBodyElement( SOAPMessage message)
			throws SOAPException, IOException, ServletException {
		for (Iterator<?> children = message.getSOAPBody().getChildElements(); children.hasNext();) {
			Object o = children.next();
			if (o instanceof Element) {
				return (Element) o;
			}
		}
		throw new ServletException("The Soap-Body does not contains an element!");
	}

	
	public Optional<Integer> readTransferHandle(SOAPMessage message) throws SOAPException {
		for (Iterator<?> attachments = message.getAttachments(); attachments.hasNext();) {
			AttachmentPart attachment = (AttachmentPart) attachments.next();
			String content = new String(attachment.getRawContentBytes()).trim();
			try {
				return Optional.of(Integer.parseInt(content));
			} catch (NumberFormatException e) {

			}
		}
		return Optional.empty();
	}


	public static XmlUtils getInstance() {
		return INSTANCE;
	}

	public String toString(Optional<Element> o) throws IOException {
		return toString(o.get());
	}
	public String toString(Element o) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.ENCODING, "utf-8");
			transformer.setOutputProperty(OutputKeys.INDENT, "no");
			transformer.transform(new DOMSource(o), new StreamResult(bos));
			return bos.toString("utf-8");
		} catch (TransformerFactoryConfigurationError | TransformerException e) {
			throw new IOException(e);
		}
	}
	

}
