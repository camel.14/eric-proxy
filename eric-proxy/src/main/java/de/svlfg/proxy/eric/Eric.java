package de.svlfg.proxy.eric;

import de.svlfg.eric.library.EricapiLibrary;

public class Eric {
	private static Eric eric;

	private EricapiLibrary ericapiLib;

	/**
	 * @see Eric#initialize(String, String)
	 */
	private Eric(EricapiLibrary ericapiLib) {
		this.ericapiLib = ericapiLib;
	}

	public static void initialize(EricapiLibrary ericapiLib) {
		eric = new Eric(ericapiLib);
	}
	

	public static Eric getInstance() {
		return eric;
	}

	public static EricapiLibrary getLibrary() {
		return eric.ericapiLib;
	}

}
