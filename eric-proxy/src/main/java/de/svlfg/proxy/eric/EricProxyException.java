package de.svlfg.proxy.eric;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;

public class EricProxyException extends Exception implements EricProxyResult {

	private int returnCode;
	private String message;

	public EricProxyException(int rc, String message) {
		super(message);
		this.returnCode = rc;
		this.message = message;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void write(HttpServletResponse resp) throws ServletException {
		try {
			SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
			String prefix = soapMessage.getSOAPBody().getElementName().getPrefix();
			SOAPFault fault = soapMessage.getSOAPBody().addFault();
			fault.setFaultCode(prefix + ":Server");
			fault.setFaultString("Error Number: " + returnCode);
			soapMessage.saveChanges();
			String type = soapMessage.getMimeHeaders().getHeader("content-type")[0];
			resp.setContentType(type);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			soapMessage.writeTo(out);
			resp.getOutputStream().write(out.toByteArray());
		} catch (SOAPException | IOException e) {
			throw new ServletException(e);
		}

	}

}
