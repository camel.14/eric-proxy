package de.svlfg.proxy.eric;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoopServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static Logger log = LoggerFactory.getLogger(LoopServlet.class);

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		log.debug("doPost");
		if (req.getPathInfo().startsWith("/error")) {
			new EricProxyException(2300111, "TestFehler").write(resp);
		} else {
			MimeHeaders header = new MimeHeaders();
			header.setHeader("content-type", req.getContentType());
			try {
				SOAPMessage message = MessageFactory.newInstance().createMessage(header, req.getInputStream());
				message.getSOAPBody();
				message.saveChanges();
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				message.writeTo(bos);
				log.debug("message received {}",bos.toString());
				resp.setContentType(message.getMimeHeaders().getHeader("content-type")[0]);
				message.writeTo(resp.getOutputStream());
			} catch (SOAPException e) {
				throw new ServletException(e);
			}
		}

	}
}
