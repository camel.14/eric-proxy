package de.svlfg.proxy.eric.dto;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class ElsterTest {

	@Test
	public void testGetDatenTeil02() throws Exception {
		String name = "ElsterDecoder02.xml";
		InputStream resourceAsStream = ElsterTest.class.getClassLoader().getResourceAsStream(name);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(false);
		Document document = factory.newDocumentBuilder().parse(resourceAsStream);
		XPath xpath = XPathFactory.newInstance().newXPath();
		NodeList e =  (NodeList) xpath.evaluate("//Datenpaket", document.getDocumentElement(), XPathConstants.NODESET);
		if (e != null && e.getLength() > 0) {
			NodeList nl = e.item(0).getChildNodes();
			StringBuilder b = new StringBuilder();
			for (int i = 0; i < nl.getLength(); i++) {
				Node n = nl.item(i);
				if (n instanceof Text) {
					b.append((((Text)n).getData()));
				}
			}
			System.out.println(b.toString());
		} else {
			throw new IOException();
		}
	}

}
