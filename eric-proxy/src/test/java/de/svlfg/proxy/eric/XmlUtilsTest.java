package de.svlfg.proxy.eric;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPMessage;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import de.elster.eric.wrapper.SoapTest;

public class XmlUtilsTest {
	
	
	private String send( String string2,Optional<Integer> transferHandle) throws Exception {
		InputStream is = SoapTest.class.getClassLoader().getResourceAsStream(string2);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		Document doc = factory.newDocumentBuilder().parse(is);
		SOAPMessage message = MessageFactory.newInstance().createMessage();

		SOAPBody soapBody = message.getSOAPBody();
		Node node = ((Element) soapBody).getOwnerDocument().adoptNode(doc.getDocumentElement());
		soapBody.appendChild(node);
		if (transferHandle.isPresent()) {
			AttachmentPart part = message.createAttachmentPart();
			byte[] b = Integer.toString(transferHandle.get()).getBytes();
			part.setRawContentBytes(b, 0, b.length, "text/plain");
			message.addAttachmentPart(part);
		}
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		message.writeTo(bos);
		return bos.toString();
	}
}
