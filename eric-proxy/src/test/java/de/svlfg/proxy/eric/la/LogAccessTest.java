package de.svlfg.proxy.eric.la;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Paths;

import org.junit.Test;

public class LogAccessTest {

	@Test
	public void test() throws IOException {
		LogAccess logAccess = new LogAccess(Paths.get("./"), Paths.get("eric.log"),"^2020-01.*");
		String content = logAccess.read();
		System.out.println(content);
	}

}
