package de.elster.eric.wrapper;

import java.io.InputStream;
import java.net.URL;
import java.util.Optional;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class DecoderTest {

	private final static String SERVER = TestConfiguration.SERVER;

	
	@Test
	public void testElsterDatenteil02() throws Exception {
		SOAPMessage resp = send(SERVER, "", "ElsterDecoder02.xml",Optional.empty());
		resp.writeTo(System.out);
	}


	private SOAPMessage send(String server2, String string, String string2,Optional<Integer> transferHandle) throws Exception {
		InputStream is = SoapTest.class.getClassLoader().getResourceAsStream(string2);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		Document doc = factory.newDocumentBuilder().parse(is);
		SOAPMessage message = MessageFactory.newInstance().createMessage();

		SOAPBody soapBody = message.getSOAPBody();
		Node node = ((Element) soapBody).getOwnerDocument().adoptNode(doc.getDocumentElement());
		soapBody.appendChild(node);
		if (transferHandle.isPresent()) {
			AttachmentPart part = message.createAttachmentPart();
			byte[] b = Integer.toString(transferHandle.get()).getBytes();
			part.setRawContentBytes(b, 0, b.length, "text/plain");
			message.addAttachmentPart(part);
		}
		message.saveChanges();
		SOAPConnection connection = SOAPConnectionFactory.newInstance().createConnection();
		URL url = new URL(server2 + "/eric-proxy/decoder" );
		SOAPMessage result = connection.call(message, url);
		return result;
	}

}
