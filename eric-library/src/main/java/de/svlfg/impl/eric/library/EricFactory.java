package de.svlfg.impl.eric.library;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.ptr.PointerByReference;
import com.sun.jna.win32.StdCallLibrary;

import de.svlfg.eric.library.EricapiLibrary;
import de.svlfg.eric.library.Errorcode;

public class EricFactory {
	private static EricFactory eric;

	private EricapiLibrary ericapiLib;
	private final static Logger log = LoggerFactory.getLogger(EricFactory.class);

	/**
	 * @see EricFactory#initialize(String, String)
	 */
	private EricFactory(final String ericPath, final String logPath) throws EricException, WrapperException {
		final String newEncoding = "UTF-8";
		final String prevEncoding = System.setProperty("jna.encoding", newEncoding);
		if (prevEncoding != null && !prevEncoding.equalsIgnoreCase(newEncoding)) {
			throw new WrapperException("Das Java-System-property jna.encoding war auf " + prevEncoding
					+ " gesetzt. Eine andere Komponente die dieses Encoding benötigt wird beeinträchtigt");
		}

		// So verhindern wir, dass der JNA sich die jnidispatch
		// Bibliothek nicht von anderswo her als dem mitgelieferten
		// Repository holt:
		System.setProperty("jna.nosys", "true");

		loadEricapi(ericPath);
		setEricSettings(ericPath, logPath);
	}

	/**
	 * Den ERiC laden und initialisieren.
	 *
	 * @param ericPath
	 *            In diesem Verzeichnis werden die ERiC Bibliotheken und die
	 *            Unterverzeichnisse „plugins“ und „plugins2“ erwartet.
	 * @param logPath
	 *            Das Verzeichnis für ERiC Log-Dateien. Es muss existieren und
	 *            beschreibbar sein. ERiC erzeugt keine Log-Dateien falls dieses
	 *            Verzeichnis nicht schreibbar ist.
	 * @throws EricException
	 *             Falls ein Fehler beim Konfigurieren von ERiC auftritt.
	 * @throws WrapperException
	 *             Falls ein Fehler im Java-Wrapper auftritt, z.B. wenn ERiC im
	 *             Verzeichnis {@code ericPath} nicht gefunden werden kann.
	 */
	public static synchronized void initialize(final String ericPath, final String logPath)
			throws EricException, WrapperException {
		eric = new EricFactory(ericPath, logPath);
	}

	/**
	 * Gibt die ERiC Singleton Instanz zurück.
	 *
	 * @return ERiC-Singleton
	 */
	public static EricapiLibrary getInstance() {
		return eric.ericapiLib;
	}

	/**
	 * Setzt eine ERiC-Einstellung.
	 *
	 * Siehe EricEinstellungSetzen in ericapi.h
	 *
	 * @param name
	 *            Name der Einstellung
	 * @param value
	 *            Neuer Wert der Einstellung
	 * @throws EricException
	 *             Falls ERiC einen Fehler zurückgibt.
	 */
	public void setSetting(final String name, final String value) throws EricException {
		final int rc = ericapiLib.EricEinstellungSetzen(name, value);
		if (rc != Errorcode.ERIC_OK.getErrorcode()) {
			throwRcException(rc);
		}
	}

	/**
	 * Setzt eine ERiC-Pfadeinstellung
	 *
	 * @param name
	 *            Name der Einstellung
	 * @param path
	 *            Neuer Wert der Einstellung
	 * @throws EricException
	 *             Falls ERiC einen Fehler zurückgibt.
	 * @throws WrapperException
	 *             Bei Fehlern in der Wrapperschicht
	 */
	public void setPath(final String name, final String path) throws WrapperException, EricException {
		final int rc = ericapiLib.EricEinstellungSetzen(name, encodePath(path));
		if (rc != Errorcode.ERIC_OK.getErrorcode()) {
			throwRcException(rc);
		}
	}

	/**
	 * Lädt die ericapi.dll/libericapi.so/libericapi.dylib und erstellt das
	 * JNA-Wrapperobjekt
	 *
	 * @param ericPath
	 *            Pfad in dem sich die ericapi.dll befindet
	 * @throws WrapperException
	 *             Bei einem Fehler, wenn die ericapi.dll nicht geladen werden kann.
	 */
	private void loadEricapi(final String ericPath) throws WrapperException {
		final String libraryName = OsHelper.getOsSharedLibraryName("ericapi");
		final Path ericapiPath = Paths.get(ericPath, libraryName);

		// if (!Files.exists(ericapiPath)) {
		// throw new WrapperException(libraryName + " wurde in Pfad \"" + ericPath + "\"
		// nicht gefunden");
		// }

		final Map<String, String> options = new HashMap<String,String>();
		// Funktioniert nicht vollständig mit JNA 4.0.0
		options.put(Library.OPTION_STRING_ENCODING, "ISO-8859-15");
		if ("32".equals(System.getProperty("sun.arch.data.model"))) {
			options.put(Library.OPTION_CALLING_CONVENTION, "" + StdCallLibrary.STDCALL_CONVENTION);
		}

		try {
			ericapiLib = (EricapiLibrary) Native.loadLibrary(ericapiPath.normalize().toString(), EricapiLibrary.class,
					options);
		} catch (UnsatisfiedLinkError e) {
			throw new WrapperException("ericapi konnte nicht geladen werden! "
					+ "Bitte stellen Sie sicher, dass ihre Java-Installation in "
					+ "der gleichen CPU-Architektur vorliegt wie ERiC (x86/AMD64).", e);
		}

		if (ericapiLib == null) {
			throw new WrapperException("Fehler beim Laden des JNA-Wrapperobjekts");
		}
	}

	/**
	 * Setzt die ERiC-Einstellungen basis.log_dir und basis.home_dir
	 *
	 * @param ericPath
	 *            basis.home_dir
	 * @param logPath
	 *            basis.log_dir
	 * @throws EricException
	 *             Fehlerrückmeldungen von ERiC
	 * @throws WrapperException
	 */
	private void setEricSettings(final String ericPath, final String logPath) throws EricException, WrapperException {
		if (logPath != null) {
			final Path ericLogFile = Paths.get(logPath, "eric.log");
			final String exceptionMessageLogPath = "Keine Schreibrechte oder ungültiger Log-Pfad \"" + logPath + "\"";
			try {
				if (!Files.isDirectory(Paths.get(logPath))
						|| (Files.notExists(ericLogFile) && Files.createFile(ericLogFile) == null)
						|| !Files.isWritable(ericLogFile)) {
					throw new WrapperException(exceptionMessageLogPath);
				}
			} catch (IOException e) {
				throw new WrapperException(exceptionMessageLogPath, e);
			}
			int rc_init = ericapiLib.EricInitialisiere(ericPath, logPath);
			//setPath("basis.log_dir", logPath);
		}

		//setPath("basis.home_dir", ericPath);
	}

	/**
	 * Wandelt einen ERiC-Fehlercode in eine EricException um
	 *
	 * @param rc
	 *            Der umzuwandelnde Fehlercode
	 * @throws EricException
	 *             Die passende Exception (mit Fehlertext) zum Fehlercode
	 */
	private void throwRcException(final int rc) throws EricException {
		final PointerByReference buffer = ericapiLib.EricRueckgabepufferErzeugen();
		try {
			ericapiLib.EricHoleFehlerText(rc, buffer);
			if (buffer == null) {
				throw new EricException(rc,String.format("Eric Error Code: %d",rc));
			}
			throw new EricException(rc, ericapiLib.EricRueckgabepufferInhalt(buffer));
		} finally {
			ericapiLib.EricRueckgabepufferFreigeben(buffer);
		}
	}

	
	/**
	 * Wandelt eine Pfadangabe in das von ERiC verlangte Encoding um
	 *
	 * @param path
	 *            Pfadangabe
	 * @return Richtig kodierter Pfad als Bytes
	 * @throws WrapperException
	 *             Bei Problemen beim umkodieren
	 */
	private byte[] encodePath(final String path) throws WrapperException {
		String encoding;
		String finalPath;
		if (OsHelper.isMacOsX()) {
			// Pfade auf Mac OS X benötigen immer UTF-8
			encoding = "UTF-8";

			// Mac OS X benutzt bei HFS+ die "decomposed form"
			// von UTF-8
			finalPath = Normalizer.normalize(path, Form.NFC);
		} else {
			// Pfade sind in der momentanen Codepage/Locale kodiert
			encoding = System.getProperty("sun.jnu.encoding");
			finalPath = path;
		}

		try {
			final byte[] pathBytes = finalPath.getBytes(encoding);
			return Arrays.copyOf(pathBytes, pathBytes.length + 1);
		} catch (UnsupportedEncodingException e) {
			throw new WrapperException("Encoding nicht gefunden: " + encoding, e);
		}
	}

	
	public int EricEinstellungSetzen(String name, String wert) {
		return ericapiLib.EricEinstellungSetzen(name, wert);
	}

	public int EricEinstellungSetzen(String name, byte[] wert) {
		return ericapiLib.EricEinstellungSetzen(name, wert);
	}

}
