package de.svlfg.eric.library;

import com.sun.jna.Library;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;
import com.sun.jna.win32.StdCallLibrary.StdCallCallback;

import java.util.Arrays;
import java.util.List;

/**
 * JNA Wrapper für ERiC basierend auf der Header-Datei ericapi.h
 *
 */
public interface EricapiLibrary extends Library {

	int ERIC_VALIDIERE = 1 << 1;
	int ERIC_SENDE = 1 << 2;
	int ERIC_DRUCKE = 1 << 5;

	/**
	 * Druckparameter-Struktur für die EBV.
	 *
	 * Siehe eric_druck_parameter_t in ericapi.h
	 */
	static class eric_druck_parameter_t extends Structure {
		public int version;
		public int vorschau;
		public int ersteSeite;
		public int duplexDruck;
		public Pointer pdfName;
		public String fussText;

		@Override
		protected List<String> getFieldOrder() {
			return Arrays.asList(new String[] { "version", "vorschau", "ersteSeite", "duplexDruck", "pdfName", "fussText" });
		}
	}

	/**
	 * Verschlüsselungsparameter.
	 *
	 * Siehe eric_verschluesselungs_parameter_t in ericapi.h
	 */
	static class eric_verschluesselungs_parameter_t extends Structure {
		public int version;
		public int zertifikatHandle;
		public String pin;
		public String abrufCode;

		@Override
		protected List<String> getFieldOrder() {
			return Arrays.asList(new String[] { "version", "zertifikatHandle", "pin", "abrufCode" });
		}
	}

	interface EricFortschrittCallback extends StdCallCallback {
		void invoke(int id, int pos, int max, Pointer benutzerDaten);
	}

	int EricBearbeiteVorgang(String datenpuffer, String datenartVersion, int bearbeitungsFlags, eric_druck_parameter_t druckParameter,
			eric_verschluesselungs_parameter_t cryptoParameter, IntByReference transferHandle, PointerByReference rueckgabePuffer,
			PointerByReference serverantwortPuffer);

	int EricEinstellungSetzen(String name, String wert);

	int EricEinstellungSetzen(String name, byte[] wert);

	int EricHoleFehlerText(int fehlerkode, PointerByReference rueckgabePuffer);

	PointerByReference EricRueckgabepufferErzeugen();

	String EricRueckgabepufferInhalt(PointerByReference handle);

	int EricRueckgabepufferLaenge(PointerByReference handle);

	int EricRueckgabepufferFreigeben(PointerByReference handle);

	int EricHoleFinanzamtLandNummern(PointerByReference handle);

	int EricHoleFinanzaemter(String finanzamtLandNummer, PointerByReference handle);

	int EricHoleFinanzamtsdaten(String bufaNr, PointerByReference handle);

	int EricPruefeSteuernummer(String steuernummer);

	int EricGetAuswahlListen(String datenartVersion, String feldkennung, PointerByReference returnBuffer);

	int EricGetHandleToCertificate(IntByReference hToken, IntByReference iInfoPinSupport, byte[] pathToKeystore);

	int EricCloseHandleToCertificate(int hToken);

	int EricRegistriereFortschrittCallback(EricFortschrittCallback funktion, Pointer benutzerDaten);

	int EricRegistriereGlobalenFortschrittCallback(EricFortschrittCallback funktion, Pointer benutzerDaten);

    int EricHoleZertifikatEigenschaften(int hToken, String pin, PointerByReference rueckgabePuffer);
    
    int EricCreateTH(String xml, String verfahren, String datenart, String vorgang, String testmerker, String herstellerId, String datenLieferant,
    		String versionClient, String publicKey, PointerByReference xmlRueckgabePuffer);
    
    
    int EricGetPublicKey(eric_verschluesselungs_parameter_t cryptoParameter, PointerByReference rueckgabePuffer);
    
    int EricGetErrormessagesFromXMLAnswer(String xml, PointerByReference transferticketPuffer, PointerByReference returncodeTHPuffer,
    		PointerByReference fehlertextTHPuffer, PointerByReference returncodesUndFehlertexteNDHXmlPuffer);
    
    int EricCheckXML(String xml, String datenartVersion, PointerByReference fehlertextPuffer);
    
    int EricDekodiereDaten(int zertifikatHandle, String pin, String base64Eingabe, PointerByReference rueckgabePuffer);

	int EricInitialisiere(String pluginPfad, String logPfad);

	int EricBeende();
}
