package de.svlfg.proxy.eric.config;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

public class UserConfigImpl implements UserConfig {


	private String certificatePin;
	private byte[] certificate;
	private String senderName;
	private String senderId;

	public UserConfigImpl(Class<?> anchor, Properties properties) throws IOException {
		this.certificatePin = Objects.requireNonNull(properties.getProperty("certificatePin"), "The certificate pin must not be null!");
		String path = Objects.requireNonNull(properties.getProperty("certificateFile"), "The certificateFile property must not be null!");
		InputStream is = Objects.requireNonNull(anchor.getClassLoader().getResourceAsStream(path), "The certificate must not be null! " + path);
		this.certificate  = loatBytes(is);
		this.senderName = Objects.requireNonNull(properties.getProperty("senderName"), "The senderName must not be null!");
		this.senderId = Objects.requireNonNull(properties.getProperty("senderId"), "The senderId must not be null!");
		
	}

	private byte[] loatBytes(InputStream is) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] b = new byte[4096];
		int len,off = 0;
		while((len = is.read(b))> 0) {
			bos.write(b, off, len);
		}
		bos.close();
		is.close();
		return bos.toByteArray();
	}

	/* (non-Javadoc)
	 * @see de.svlfg.proxy.eric.UserConfig#getCertificatePin()
	 */
	@Override
	public String getCertificatePin() {
		return certificatePin;
	}

	/* (non-Javadoc)
	 * @see de.svlfg.proxy.eric.UserConfig#getCertificate()
	 */
	@Override
	public byte[] getCertificate() {
		return certificate;
	}

	/* (non-Javadoc)
	 * @see de.svlfg.proxy.eric.UserConfig#getSenderName()
	 */
	@Override
	public String getSenderName() {
		return senderName;
	}

	/* (non-Javadoc)
	 * @see de.svlfg.proxy.eric.UserConfig#getSenderId()
	 */
	@Override
	public String getSenderId() {
		return senderId;
	}

}
