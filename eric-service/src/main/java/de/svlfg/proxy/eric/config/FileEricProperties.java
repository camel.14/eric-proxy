package de.svlfg.proxy.eric.config;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

public class FileEricProperties implements EricProperties{
	
	private UserConfigImpl userConfig;
	public FileEricProperties(Class<?> anchor,String resourceName) throws IOException {
		Properties properties = initProperties(anchor, resourceName);
		initPropertiesEricLibrary(properties);
		initPropertiesUserConfig(anchor,properties);
	}

	private void initPropertiesUserConfig(Class<?> anchor, Properties properties) throws IOException {
		this.userConfig = new UserConfigImpl(anchor,properties);
		
	}

	private Map<String,String> communicationProperties = new HashMap<String,String>();
	@Override
	public UserConfig getUserConfig() {
		return userConfig;
	}
	private Properties initProperties(Class<?> anchor,String resourceName) throws IOException  {
		Properties properties = new Properties();
		InputStream resource = anchor.getClassLoader().getResourceAsStream(resourceName);
		properties.load(new InputStreamReader(resource,"utf-8"));
		return properties;
	}

	@Override
	public Collection<Entry<String, String>> getCommunicationProperties() {
		return this.communicationProperties.entrySet();
	}

	private void initPropertiesEricLibrary(Properties properties) {
		for (Enumeration<Object> keys = properties.keys(); keys.hasMoreElements();) {
			String key = keys.nextElement().toString();
			if (key.startsWith("ericlib.")) {
				communicationProperties.put(key.substring(8), properties.getProperty(key));
			}
		}

	}

}
