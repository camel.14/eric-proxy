package de.svlfg.proxy.eric.config;

import java.util.Collection;
import java.util.Map;

public interface EricProperties {
	UserConfig getUserConfig();
	Collection<Map.Entry<String,String>> getCommunicationProperties();
}