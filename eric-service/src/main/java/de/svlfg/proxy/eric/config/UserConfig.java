package de.svlfg.proxy.eric.config;


public interface UserConfig {

	String getCertificatePin();

	byte[] getCertificate();

	String getSenderName();

	String getSenderId();

}