Dieses Projekt nutzt den OSGI Standard und ist für VIRGO als Laufzeitumgebung entwickelt.

In OSGI gibt es Bundles die Services bereitstellen und Services von anderen Bundels konsumieren. 

- eric-library   -> kapselt die Libraries als Java Service
- eric-proxy     -> ist ein spezielles Bundle, das von Virgo als WebApp interpretiert wird. Hier gibt es Servlets, die auf die Services der anderen Bundles zugreifen.
- eric-services  -> stellt einige Klassen bereit, die von anderen Bundels benötigt werden.
- eric-service-prod -> stellt den Config Service für die Prod bereit
- eric-service-test -> stellt den Config Service für die Test bereit

Der Build erzeugt zwei Images 
- Prod enthält eric-service-prod
- Test enthält eric-service-test

Ein Jar File benötigt spezielle Einträge in der MANIFEST Datei. Diese Einträge sind in dem build.gradle File definiert. 

Der Lifecycle eines OSGI Bundles wird im Activator festgelegt. In der build.gradle wird der Activator definiert.


```
docker run --restart always -d --name eric-test --network bridge \
           -l "traefik.docker.network=bridge" \
           -l "traefik.frontend.rule=Host:eric-test.rz.lsv.de" \
           -l "traefik.frontend.entryPoints=https,http" \
           -l "traefik.backend=eric-test" \
             nexus-pi.rz.lsv.de:8081/dav-all-eric-proxy-test:feature
```

```
docker run --restart always -d --name eric-test --network bridge \
           -l "traefik.docker.network=bridge" \
           -l "traefik.frontend.rule=Host:eric-test.rz.lsv.de" \
           -l "traefik.frontend.entryPoints=https,http" \
           -l "traefik.backend=eric-test" \
             registry.svlfg.de/dav-all-eric-proxy-test:develop

´´´

```
docker run --restart always -d --name eric-prod --network bridge \
           -l "traefik.docker.network=bridge" \
           -l "traefik.frontend.rule=Host:eric-prod.rz.lsv.de" \
            nexus-pi.rz.lsv.de:8081/dav-all-eric-proxy-prod
```

Die Übergabe an die Produktionssteuerung erfolgt über folgenden Befehl:

```
 docker push  nexus-pi.rz.lsv.de:8081/dav-all-eric-proxy-prod:xxxx
```
